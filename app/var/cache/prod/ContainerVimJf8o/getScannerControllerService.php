<?php

namespace ContainerVimJf8o;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getScannerControllerService extends App_KernelProdContainer
{
    /*
     * Gets the public 'App\Controller\ScannerController' shared autowired service.
     *
     * @return \App\Controller\ScannerController
     */
    public static function do($container, $lazyLoad = true)
    {
        $a = ($container->services['monolog.logger.mailer'] ?? $container->load('getMonolog_Logger_MailerService'));

        $container->services['App\\Controller\\ScannerController'] = $instance = new \App\Controller\ScannerController(new \App\Service\MailService(new \Symfony\Component\Mailer\Mailer((new \Symfony\Component\Mailer\Transport(new RewindableGenerator(function () use ($container) {
            yield 0 => $container->load('getMailer_TransportFactory_NullService');
            yield 1 => $container->load('getMailer_TransportFactory_SendmailService');
            yield 2 => $container->load('getMailer_TransportFactory_NativeService');
            yield 3 => $container->load('getMailer_TransportFactory_SmtpService');
        }, 4)))->fromStrings(['main' => $container->getEnv('MAILER_DSN')]), NULL, ($container->services['event_dispatcher'] ?? $container->getEventDispatcherService())), $a, $container->getEnv('resolve:FROM_ADDRESS'), $container->getEnv('resolve:TO_ADDRESS')), $a);

        $instance->setContainer(($container->privates['.service_locator.W9y3dzm'] ?? $container->load('get_ServiceLocator_W9y3dzmService'))->withContext('App\\Controller\\ScannerController', $container));

        return $instance;
    }
}
