<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/pages/privacy-policy.html.twig */
class __TwigTemplate_2aa897a54132776b302b44a2c39c34db61948b6bcd73d6a85aac5c244ecc9d00 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "landing/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("landing/base.html.twig", "landing/pages/privacy-policy.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Политика конфиденциальности сайта";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"content--page\">
        <div id=\"allrecords\" class=\"t-records\" style=\"overflow: hidden;\">
            <style>
                main {
                    font-size: 16px;
                }

                article {
                    padding: 16px 0
                }

                h3 {
                    margin: 0 0 12px;
                    font-size: 22px;
                }

                ul.figure--list {
                    margin: 6px 0 18px 6px;
                }

                p {
                    margin: 0 0 12px 0;
                }
            </style>

            <main class=\"container\">
                <h1 class=\"h1\">Политика конфиденциальности</h1>

                <article>
                    <p>Компания EFSOL заботится о вашей конфиденциальности, поэтому мы разработали политику,
                        которая регулирует сбор, использование, раскрытие, передачу и хранение личной информации
                        пользователей.
                        В дополнение к этой политике мы предоставляем информацию о данных и конфиденциальности,
                        встроенную в
                        наши продукты для определенных функций, работа которых требует использования персональных
                        данных.
                    </p>
                    <p>Уделите несколько секунд ознакомлению с нашей политикой конфиденциальности и свяжитесь с нами,
                        если
                        возникнут вопросы.</p>
                </article>

                <article>
                    <h3>Сбор и использование личной информации</h3>
                    <p>Личной информацией являются данные, которые можно использовать для установления личности человека
                        или
                        для
                        связи с ним.</p>
                    <p>Если вы обратитесь в EFSOL, вас могут попросить предоставить личную информацию. Компания EFSOL и
                        её
                        дочерние компании могут обмениваться этой личной информацией друг с другом и использовать её в
                        соответствии с данной политикой конфиденциальности. Эти сведения могут быть объединены с другими
                        в
                        целях
                        предоставления и улучшения наших продуктов, услуг, контента и рекламы. Вы не обязаны
                        предоставлять
                        личную информацию по нашему запросу, однако, если вы её не укажете, в некоторых случаях мы не
                        сможем
                        предоставить вам продукт, оказать услугу или ответить на запрос.</p>
                    <p>Вот несколько примеров того, какую личную информацию может собирать и использовать EFSOL.</p>
                </article>

                <article>
                    <h3>Какую личную информацию мы собираем:</h3>
                    <ul class=\"figure--list\">
                        <li>При покупке продукта, загрузке обновления программы, подключении к нашим службам, обращении
                            к
                            нам
                            или участии в интернет-опросе мы можем попросить вас предоставить ту или иную информацию,
                            включая
                            имя и фамилию, почтовый адрес, номер телефона, адрес электронной почты, предпочтительный
                            способ
                            связи, идентификаторы устройства, IP-адрес, информацию о местоположении и данные кредитной
                            карты.
                        </li>
                        <li>Когда вы делитесь контентом с друзьями и членами семьи, используя продукты EFSOL,
                            отправляете
                            подарочные сертификаты или продукты и приглашаете кого-либо на форум или в другую службу
                            EFSOL,
                            компания может собирать указанную вами информацию об этих людях, в том числе их имя и
                            фамилию,
                            почтовый адрес, электронную почту и номер телефона. Компания EFSOL будет использовать эту
                            информацию
                            для ответа на ваши запросы, предоставления вам соответствующих продуктов и услуг или в целях
                            борьбы
                            с мошенничеством.
                        </li>
                        <li>В некоторых странах в определенных случаях мы имеем право попросить вас предоставить данные
                            удостоверения личности государственного образца.
                        </li>
                    </ul>
                </article>

                <article>
                    <h3>Как мы используем личную информацию:</h3>
                    <p>Мы можем обрабатывать вашу личную информацию в целях, описанных в этой политике
                        конфиденциальности, с
                        вашего согласия для соблюдения юридических обязательств, которые несёт компания EFSOL, или когда
                        это
                        необходимо в легитимных интересах компании EFSOL или третьей стороны, которой может
                        потребоваться
                        раскрыть информацию.</p>
                    <ul class=\"figure--list\">
                        <li>Собранная нами личная информация помогает держать вас в курсе новостей о новинках EFSOL,
                            обновлениях
                            программного обеспечения и предстоящих событиях.
                        </li>
                        <li>Мы также используем личную информацию с целью создания, разработки, эксплуатации,
                            распространения и
                            улучшения наших продуктов, услуг, материалов и рекламы, а также в целях предотвращения
                            потерь и
                            противодействия мошенничеству. Мы также можем использовать вашу личную информацию в целях
                            учёта
                            и
                            обеспечения безопасности сети, в том числе для защиты наших служб в интересах всех наших
                            пользователей. Мы ограничиваем своё использование данных для предотвращения мошенничества
                            лишь
                            теми,
                            которые строго необходимы и затрагивают наши легитимные интересы в связи с защитой клиентов
                            и
                            служб.
                        </li>
                        <li>Мы можем использовать вашу личную информацию, в том числе дату рождения, для проверки вашей
                            личности, идентификации пользователя и подбора подходящих для вас услуг.
                        </li>
                        <li>Мы можем использовать личную информацию для внутренних целей, например, при проведении
                            аудита,
                            анализа данных и исследований, направленных на улучшение продуктов и услуг EFSOL, а также
                            способов
                            общения с клиентами.
                        </li>
                        <li>Если вы принимаете участие в конкурсе или похожем мероприятии, мы можем использовать
                            предоставленную
                            вами информацию при организации этих событий.
                        </li>
                    </ul>
                </article>

                <article>
                    <h3>Источники личной информации, когда мы получаем её не от вас</h3>
                    <p>Мы можем получать вашу личную информацию от других людей, если они отправляли вам подарочные
                        сертификаты
                        и продукты или приглашали вас подключаться к службам EFSOL. В целях обеспечения безопасности и
                        предотвращения мошенничества мы также можем проверять информацию, предоставленную вами при
                        заключении
                        договора на обслуживание с привлечением третьей стороны, а также регистрационных записей в ЕГРЮЛ
                        и
                        прочее.</p>
                </article>

                <article>
                    <h3>Сбор и использование неличной информации</h3>
                    <p>Мы также осуществляем сбор данных, которые не относятся непосредственно к конкретному человеку.
                        Мы
                        можем
                        собирать, использовать, передавать и раскрывать такие сведения с любой целью. Вот несколько
                        примеров
                        того, какую информацию, не являющуюся личной, мы собираем и используем.</p>
                    <ul class=\"figure--list\">
                        <li>Мы можем собирать такую информацию, как место работы, язык, почтовый индекс, код города,
                            уникальный
                            идентификатор устройства, URL-адрес источника ссылки, местоположение и часовой пояс, в
                            котором
                            используется продукт EFSOL. Это помогает нам лучше понять потребности пользователей и
                            предлагать
                            им
                            более подходящие продукты, услуги и рекламу.
                        </li>
                        <li>Мы можем собирать информацию о действиях клиентов на нашем веб-сайте. Кроме того, мы можем
                            использовать данные, полученные благодаря другим нашим продуктам и сервисам. Данные сведения
                            позволяют нам предоставлять клиентам более полезную информацию, а также понять, какие
                            разделы
                            веб-сайта, продукты и сервисы пользуются повышенным интересом. Данные, скомпонованные в
                            рамках
                            этой
                            политики конфиденциальности, не относятся к категории личной информации.
                        </li>
                        <li>Мы можем собирать и хранить сведения о том, каким образом вы используете наши сервисы,
                            включая
                            поисковые запросы. Эта информация может использоваться в целях повышения релевантности
                            предоставляемых нами услуг. Подобная информация не будет связана с вашим IP-адресом, за
                            исключением
                            некоторых случаев, когда это необходимо для обеспечения качества услуг, предоставляемых нами
                            через
                            Интернет.
                        </li>
                        <li>При наличии вашего явно выраженного согласия мы можем собирать сведения о том, каким образом
                            вы
                            используете наши продукты и приложения, с тем чтобы помочь разработчикам улучшить эти
                            приложения.
                        </li>
                    </ul>
                    <p>При объединении информации, не являющейся личной, с личной информацией объединённые данные будут
                        считаться личными, пока они остаются объединенными.</p>
                </article>

                <article>
                    <h3>Файлы cookie и другие технологии</h3>
                    <p>Веб-сайты, онлайн-службы, интерактивные приложения, сообщения электронной почты и рекламные
                        объявления
                        EFSOL могут использовать файлы \"cookies\" и другие технологии, такие как пиксельные теги и
                        веб-маяки.
                        Эти
                        технологии позволяют нам лучше понять действия пользователей, узнать, какие страницы наших
                        веб-сайтов
                        они посещают, а также оценить и повысить эффективность рекламы и веб-поиска. Информация,
                        собранная
                        при
                        помощи cookie и других подобных технологий, не считается личной. Однако IP-адреса или
                        аналогичные
                        идентификаторы относятся к личной информации, если это предусмотрено местным законодательством.
                        Аналогично в контексте данной политики конфиденциальности при объединении информации, не
                        являющейся
                        личной, и личной информации мы считаем такую информацию личной.</p>
                    <p>Компания EFSOL и её партнеры также вправе использовать файлы cookie и другие технологии для
                        запоминания
                        личной информации при посещении вами веб-сайтов, онлайн-сервисов и приложений. В таких случаях
                        наша
                        цель
                        заключается в том, чтобы сделать ваше взаимодействие с EFSOL более удобным и личным. Сведения о
                        покупке
                        определенного продукта или использовании конкретного сервиса с помощью вашего компьютера или
                        мобильного
                        устройства помогут сделать наши объявления и электронные письма в большей степени
                        соответствующими
                        вашим
                        интересам. Кроме того, ваша контактная информация, идентификаторы оборудования и сведения о
                        вашем
                        компьютере или устройстве помогут нам персонализировать вашу операционную систему и обеспечить
                        оптимальное качество обслуживания.</p>
                    <p>Чтобы отключить файлы cookie в веб-браузере Google Chrome, откройте настройки Google Chrome,
                        кликните
                        по
                        ссылке «Показать дополнительные настройки» и нажмите «Настройки контента». Установите флажок
                        “Удалять
                        локальные данные при закрытии браузера”. Если вы используете другой веб-браузер, свяжитесь со
                        своим
                        провайдером, чтобы узнать, как отключить файлы cookie. Следует учесть, что после отключения
                        файлов
                        cookie некоторые функции веб-сайта EFSOL станут недоступны.</p>
                    <p>Как и большинство интернет-сервисов, мы автоматически собираем определенную информацию и храним
                        её в
                        файлах журналов. К такой информации относятся ваш IP-адрес, тип браузера и язык интерфейса,
                        название
                        интернет-провайдера, адреса веб-сайтов или названия приложений, с которых и на которые был
                        совершен
                        переход, операционная система, метки даты и время, сведения о посещениях.</p>

                    <p>Мы используем эту информацию для понимания и анализа тенденций, для управления сайтом, изучения
                        поведения
                        пользователей на сайте, улучшения наших продуктов и услуг и для сбора демографической информации
                        о
                        наших
                        пользователях в целом. Компания EFSOL может использовать эту информацию в маркетинговых и
                        рекламных
                        сервисах.</p>

                    <p>В некоторых наших сообщениях электронной почты мы используем «URL-адреса перехода по щелчку»,
                        связанные с
                        материалами на веб-сайте EFSOL. Когда пользователи нажимают на такие ссылки, они переходят на
                        целевую
                        страницу через отдельный веб-сервер. Мы отслеживаем данные о таких кликах, так как они помогают
                        нам
                        определять наиболее популярные темы и оценивать эффективность взаимодействия с пользователями.
                        Если
                        вы
                        хотите отказаться от такого отслеживания, не переходите по текстовым и графическим ссылкам в
                        письме.</p>

                    <p>Пиксельные теги позволяют отправлять электронные письма в доступном для пользователей формате, а
                        также
                        дают возможность определить, было ли открыто и прочитано письмо. Мы можем использовать эти
                        сведения,
                        чтобы уменьшить количество электронных писем или отказаться от них.</p>
                </article>

                <article>
                    <h3>Раскрытие информации третьим лицам</h3>
                    <p>Иногда компания EFSOL может раскрывать некоторые личные данные пользователей своим стратегическим
                        партнерам, которые помогают предоставлять продукты и услуги компании или продвигают продукцию
                        EFSOL
                        на
                        рынке. Компания EFSOL предоставляет личную информацию только для распространения или улучшения
                        продуктов, услуг и рекламы; эта информация не передается третьим лицам в маркетинговых
                        целях.</p>
                </article>

                <article>
                    <h3>Сервисные центры</h3>
                    <p>EFSOL обменивается личными данными пользователей с компаниями, которые предоставляют такие
                        услуги,
                        как
                        обработка информации, продление срока кредита, выполнение заказов клиентов, доставка продуктов,
                        управление данными клиентов и их расширение, обслуживание клиентов, оценка заинтересованности
                        пользователей в наших продуктах и услугах, а также проведение исследований и опросов с целью
                        определения
                        степени удовлетворенности. Эти компании обязуются защищать ваши данные и могут быть расположены
                        в
                        тех
                        странах, в которых компания EFSOL ведёт свою деятельность.</p>
                </article>

                <article>
                    <h3>Иные стороны</h3>
                    <p>Компания EFSOL может быть обязана раскрыть вашу личную информацию в соответствии с
                        законодательством,
                        требованиями судопроизводства, судебного разбирательства или по запросу государственных органов
                        страны
                        вашего пребывания или других стран. Мы также можем раскрыть вашу личную информацию, если это
                        будет
                        необходимо в целях национальной безопасности, исполнения закона или в иных общественно важных
                        целях.</p>
                    <p>Мы также можем раскрыть вашу личную информацию, если это будет необходимо для выполнения наших
                        условий
                        или защиты наших операций или пользователей. Кроме того, в случае реорганизации, слияния или
                        продажи
                        компаний мы можем передавать личные данные соответствующей третьей стороне.</p>
                </article>

                <article>
                    <h3>Существование автоматизированного принятия решений, включая профилирование</h3>
                    <p>EFSOL не принимает никаких решений с использованием алгоритмов или профилирования, в значительной
                        мере
                        затрагивающих пользователя.</p>
                </article>

                <article>
                    <h3>Целостность и хранение личной информации</h3>
                    <p>С помощью EFSOL вы легко сможете обеспечить точность, полноту и актуальность личной информации.
                        Мы
                        будем
                        хранить вашу личную информацию в течение периода, необходимого для выполнения целей,
                        перечисленных в
                        данной политике конфиденциальности, и для сводных отчётов о конфиденциальности для конкретных
                        служб.
                        При
                        оценке длительности таких периодов мы тщательно изучаем свою потребность в отношении сбора
                        личной
                        информации и, когда такая потребность есть, мы храним информацию в течение минимального
                        возможного
                        периода для достижения цели сбора, если закон не обязывает нас хранить её дольше.</p>
                </article>
                <article>
                    <h3>Доступ к личной информации</h3>
                    <p>Мы предоставим вам доступ к ним и их копию в любом случае, в том числе если вы хотите исправить
                        ошибки в
                        информации или попросить стереть её — мы исполним такую просьбу, если компания EFSOL не обязана
                        хранить
                        данные по закону или они не нужны ей для законных деловых целей. Мы можем отказать в исполнении
                        запросов, которые являются безосновательными или недобросовестными, нарушают конфиденциальность
                        других
                        пользователей, не имеют смысла или доступ к которым в соответствии с местным законодательством
                        не
                        является обязательным. Мы также можем отклонять аспекты запросов на удаление или доступ, если
                        считаем,
                        что это поставит под сомнение наше законное использование данных в целях предотвращения
                        мошенничества и
                        обеспечения безопасности, как описано выше. Онлайн-инструменты для отправки запросов на доступ,
                        исправление или удаление предоставляются на региональном уровне. </p>
                </article>

                <article>
                    <h3>Сервисы на основе данных о местоположении</h3>
                    <p>Для предоставления сервисов на основе данных о местоположении в своих продуктах компания EFSOL, а
                        также
                        наши партнёры могут собирать данные о местоположении, использовать их и обмениваться ими. Такие
                        сведения
                        включают данные о географическом положении вашего компьютера, получаемые в режиме реального
                        времени.
                        Сервисы, основанные на местоположении, могут использовать (если применимо) данные GPS, Bluetooth
                        и
                        ваш
                        IP-адрес, а также местоположение точек доступа WI-Fi и вышек сотовой связи, указанное
                        добровольцами,
                        и
                        другие технологии для определения приблизительного местоположения вашего устройства. За
                        исключением
                        случаев, когда вы дали явное согласие, данные о вашем местоположении собираются анонимно в такой
                        форме,
                        которая не позволяет установить вашу личность, и используются компанией EFSOL и нашими
                        партнёрами в
                        целях предоставления и улучшения продуктов и услуг, использующих данные о местоположении. </p>
                </article>

                <article>
                    <h3>Сайты и сервисы сторонних поставщиков</h3>
                    <p>Веб-сайты, продукты, сервисы и приложения EFSOL могут содержать ссылки на сторонние сайты,
                        продукты и
                        сервисы. В рамках наших продуктов и сервисов также могут использоваться или предлагаться
                        продукты
                        или
                        сервисы наших партнёров.</p>
                    <p>Управление информацией, которую собирают сторонние разработчики и которая может включать данные о
                        местонахождении или контактную информацию, регулируется политиками конфиденциальности этих
                        сторонних
                        компаний. Мы рекомендуем ознакомиться с политикой конфиденциальности таких третьих сторон.</p>
                </article>

                <article>
                    <h3>Соблюдение конфиденциальности данных пользователей на уровне компании</h3>
                    <p>Для обеспечения защиты личной информации мы знакомим сотрудников EFSOL с рекомендациями по
                        безопасности и
                        конфиденциальности и обеспечиваем строгое их исполнение в компании.</p>
                </article>

                <article>
                    <h3>Вопросы о конфиденциальности</h3>
                    <p>Если у вас возникают вопросы или опасения по поводу политики конфиденциальности EFSOL или по
                        поводу
                        обработки данных, если вы хотите обратиться к нашему директору по защите данных или подать
                        жалобу о
                        возможном нарушении местного законодательства о конфиденциальности, свяжитесь с нами. С нами
                        можно в
                        любое время связаться, позвонив по соответствующему номеру для вашей страны или региона.</p>
                    <p>Все обращения по вопросам конфиденциальности, а также о личной информации, полученной по запросу
                        на
                        доступ к данным или их загрузку, передаются в специальный отдел, сотрудники которого оценивают
                        их и
                        пытаются найти решение проблемы. Если ваша проблема требует более детального рассмотрения, они
                        могут
                        обратиться к вам для получения дополнительных сведений. Отправителям таких нестандартных
                        обращений
                        предоставляется подробный ответ. Если вы недовольны полученным ответом, направьте жалобу в
                        соответствующий регулятивный орган своей страны. Чтобы узнать, как подать жалобу в каждом
                        конкретном
                        случае, свяжитесь с нами.</p>
                    <p>Компания EFSOL периодически обновляет политику конфиденциальности. В случае внесения существенных
                        изменений в настоящую политику на нашем веб-сайте будет размещено соответствующее уведомление, а
                        также
                        опубликована обновленная политика.</p>
                </article>
            </main>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "landing/pages/privacy-policy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/pages/privacy-policy.html.twig", "/symfony/templates/landing/pages/privacy-policy.html.twig");
    }
}
