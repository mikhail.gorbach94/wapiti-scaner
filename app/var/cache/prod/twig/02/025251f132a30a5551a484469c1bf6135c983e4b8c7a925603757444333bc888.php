<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/pages/contacts.html.twig */
class __TwigTemplate_c2311e75d9577e6c92a34b48bb0bab74b68fc5718ec6ee84559622cb5a2e9a26 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "landing/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("landing/base.html.twig", "landing/pages/contacts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "<title>Контакты</title>";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"content--page\">
        <div id=\"allrecords\" class=\"t-records\" style=\"overflow: hidden;\">
            <div id=\"content\">
                <div id=\"default-tab\" class=\"b-stripe b-stripe_content_base b-stripe_type_overhang contacts--page\">
                    <div class=\"b-stripe__in\">
                        <div class=\"container\">
                            <div class=\"row\">
                                <h1 class=\"h1 wow fadeIn full-opacity\" data-wow-duration=\".5s\"
                                    style=\"visibility: visible; animation-duration: 0.5s; animation-name: fadeIn;\">
                                    <span>Офисы в Москве</span>
                                </h1>
                            </div>
                        </div>

                        <link rel=\"stylesheet\" type=\"text/css\"
                              href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/efsol_style.css"), "html", null, true);
        echo "\">

                        <div class=\"e_page\">
                            <div class=\"contacts--container mb-104 row\">
                                <div class=\"col-md-6\">
                                    <div class=\"contacts--address\">
                                        <div class=\"container--contacts--address\">
                                            <h3 class=\"h3-yellow-small grey-figure wow fadeIn\" data-wow-duration=\".5s\"
                                                style=\"visibility: visible; animation-duration: 0.5s; animation-name: fadeIn;\">
                                                <span>Центральный офис</span>
                                            </h3>
                                            <p>Адрес: 117218, г. Москва, улица Кедрова, дом&nbsp;14, корпус&nbsp;2, этаж
                                                5&nbsp;пом. I&nbsp;ком. 1-12</p>
                                            <a href=\"tel:+74952300303\" class=\"tel--number\">+7&nbsp;495&nbsp;230&nbsp;03&nbsp;03</a>
                                            <p class=\"mail\"><a href=\"mailto:efsol@efsol.ru\"
                                                               onmouseover=\"\$(this).attr('href', 'mailto:efsol@efsol.ru')\"
                                                >efsol@efsol.ru</a>&nbsp;(общий)
                                            </p>
                                            <p class=\"mail mb-0\"><a href=\"mailto:staff@efsol.ru\"
                                                                    onmouseover=\"\$(this).attr('href', 'mailto:staff@efsol.ru')\"
                                                >staff@efsol.ru</a>&nbsp;(для
                                                резюме)</p>

                                            <h3 class=\"h3-yellow-small grey-figure wow fadeIn mt-64\"
                                                data-wow-duration=\".5s\"
                                                style=\"visibility: visible; animation-duration: 0.5s; animation-name: fadeIn;\">
                                                <span>График работы</span>
                                            </h3>
                                            <p class=\"schedule\">Пн - Пт: с 10.00 - 19.00</p>
                                            <p class=\"schedule mb-0\">Сб - Вс: выходной</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"col-md-6 contacts--map\">
                                    <div id=\"YaMaps2\" style=\"width: 100%; height: 100%;\">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src=\"https://api-maps.yandex.ru/2.1/?apikey=821da493-0e7c-47f8-bcb7-a47e7f5774cc&lang=ru_RU\" type=\"text/javascript\">
    </script>
    <script>
        ymaps.ready(function () {
            let zoom = 15,
                mark = [55.682991,37.572082];

            let myMap = new ymaps.Map('YaMaps2', {
                center: mark,
                zoom: zoom,
                controls: ['zoomControl'],
            });

            myMap.geoObjects.add(new ymaps.Placemark(mark, {balloonContent: 'Кедрова 14, корпус 2'}));
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "landing/pages/contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 21,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/pages/contacts.html.twig", "/symfony/templates/landing/pages/contacts.html.twig");
    }
}
