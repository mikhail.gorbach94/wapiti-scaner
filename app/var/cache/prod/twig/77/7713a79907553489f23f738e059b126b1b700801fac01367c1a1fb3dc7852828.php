<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/pages/about.html.twig */
class __TwigTemplate_dfd92f29d21c3ed798e2d6ae1ed5cc03f39862b3f10db5639519927a8e90cd1f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "landing/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("landing/base.html.twig", "landing/pages/about.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "<title>О сервисе</title>";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"content--page\">
        <div class=\"inner__page\">
            <main class=\"b-stripe b-stripe_content_base inner__page__inner company-page\">
                <div class=\"news-detail\">
                    <div class=\"b-stripe__in__1440\">

                        <div class=\"b-stripe__in\">
                            <h1 class=\"h1 wow fadeIn full-opacity\" data-wow-duration=\".5s\"
                                style=\"visibility: visible; animation-duration: 0.5s;\">
                                <span>О сервисе</span>
                            </h1>
                        </div>

                        <div class=\"b-stripe__in__1440\">
                            <div class=\"e_page\">
                                <section class=\"container__products mt-40\">
                                    <div class=\"container b-stripe b-stripe_content_item-promo-lede slide-4 banner--sl\">
                                        <div class=\"animated fone-for-slider\">
                                            <div class=\"fn-dark-slider-left left-sd grey-sl-left\"></div>

                                            <div class=\"wow slideInRight fn__forSlider yellow-bg-sl fone-for-slider anim-fn right-sd\"
                                                 data-wow-duration=\"2s\"
                                                 style=\"visibility: visible; animation-duration: 2s;\"></div>

                                            <div class=\"triangel-right tring-dark fone-for-slider anim-fn\"
                                                 data-wow-duration=\"2.5s\" data-wow-offset=\"0\"></div>
                                        </div>

                                        <div class=\"fone-for-slider-body\">
                                            <div class=\"b-stripe__in\">
                                                <div class=\"b-section b-section_content_promo-lede\">
                                                    <div class=\"b-section__body\">
                                                        <div class=\"b-section__summary color-black\">
                                                            <b>EFSOL Scanner</b> осуществляет проверку сайта на
                                                            уязвимости.
                                                            <br>
                                                            Сканирование сайта на уязвимости - необходимая мера, которая
                                                            позволяет оценить уровень его защищенности от угроз
                                                            компроментации.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=\"container audit_important mb-104 pr-0 pl-0\">
                                        <h2 class=\"h1 wow fadeIn full-opacity\" data-wow-duration=\".5s\"
                                            style=\"visibility: hidden; animation-duration: 0.5s; animation-name: none;\">
                                            <span>Почему так важно проводить аудит безопасности сайта?</span>
                                        </h2>

                                        <p>Многие владельцы бизнеса, которым повезло не столкнуться с проблемами
                                            несанкционированного доступа к их ресурсам, не осознают, зачем нужен аудит
                                            безопасности.<br>
                                            Ниже перечислены возможные варианты того, что может произойти после взлома:
                                        </p>
                                        <div class=\"block--filled\" style=\"margin-top: 25px;\">
                                            <div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Сайт нельзя будет восстановить:</div>
                                                        <p class=\"mb-0\">
                                                            Хакер, получивший доступ к сайту, может вывести его из строя
                                                            или полностью уничтожить без возможности восстановления.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Потеря звонков, заявок, продаж</div>
                                                        <p class=\"mb-0\">
                                                            В то время, пока веб-ресурс будет находиться в состоянии
                                                            блокировки, его страницы могут выпасть из индекса
                                                            поисковиков, потерять высокие позиции по нужным ключевым
                                                            словам и потенциальные клиенты не будут его находить в
                                                            выдаче.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Блокировка от хостера</div>
                                                        <p class=\"mb-0\">
                                                            Хостинг-провайдер, обнаружив вредоносный код в ходе плановой
                                                            проверки, заблокирует доступ к сайту или даже ко всему
                                                            аккаунту хостинга. Таким образом, все посетители, которые
                                                            будут заходить на интернет-площадку в период блокировки,
                                                            увидят только «заглушку» хостера и статус ошибки 503.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Блокировка от поисковиков</div>
                                                        <p class=\"mb-0\">
                                                            Если хостинг-провайдер не заблокирует интернет-площадку за
                                                            вредоносный код, его может обнаружить поисковая система, и
                                                            тогда в результатах поисковой выдачи рядом со ссылкой будет
                                                            добавляться предупреждение для пользователей, что данный
                                                            сайт может угрожать безопасности компьютера — само собой,
                                                            это негативно отразится на статистике посещаемости.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Браузерная блокировка</div>
                                                        <p class=\"mb-0\">
                                                            Взломанный и зараженный сайт может быть заблокирован не
                                                            только хостером, но и браузерами (например, Chrome,
                                                            Яндекс.Браузер и Opera).
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Штрафы, суды</div>
                                                        <p class=\"mb-0\">
                                                            Если компания через сайт работает с конфиденциальными
                                                            данными пользователей или клиентов, то из-за действий
                                                            хакеров эта информация может попасть в руки мошенников.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class=\"etaps__v1 bg-grey flex mb-32\">
                                                    <div class=\"container--etaps\">
                                                        <div class=\"title\">Падение прибыли</div>
                                                        <p class=\"mb-0\">
                                                            Из-за взлома бизнес может потерять
                                                            потенциальных клиентов и доверие постоянных
                                                            покупателей. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=\"container vulnerabilities pr-0 pl-0\">
                                        <h2 class=\"h1 wow fadeIn full-opacity\" data-wow-duration=\".5s\"
                                            style=\"visibility: hidden; animation-duration: 0.5s; animation-name: none;\">
                                            <span>Анализируемые уязвимости</span>
                                        </h2>
                                        <div class=\"position-relative mb-24\">
                                            <div class=\"row col-gutters-12\">
                                                <div class=\"col-md-6 col-gutters-12_col\">
                                                    <div class=\"pr-md-2 pr-lg-5\">
                                                        <ul class=\"efsol_goldUL\">
                                                            <li>раскрытие файла (локальные и удаленные, fopen,
                                                                readfile);
                                                            </li>
                                                            <li>инъекции (PHP / JSP / ASP / SQL-инъекции и
                                                                XPath-инъекции);
                                                            </li>
                                                            <li>XSS (межсайтовый скриптинг) (отраженная и постоянная);
                                                            </li>
                                                            <li>обнаружение и выполнение команд (eval(), system(),
                                                                passtru() );
                                                            </li>
                                                            <li>CRLF-инъекции (разделение ответов HTTP, фиксация
                                                                сеанса);
                                                            </li>
                                                            <li>XXE (XML внешняя сущность) внедрение;</li>
                                                            <li>SSRF (подделка запроса на стороне сервера);</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 col-gutters-12_col\">
                                                    <div class=\"h-100 w-100 position-relative d-flex flex-column justify-content-between tm-overflow-hidden\">
                                                        <div class=\"position-relative pb-32 pr-md-1 pr-md-2 px-lg-4\">
                                                            <ul class=\"efsol_goldUL\">
                                                                <li>использование известных потенциально опасных
                                                                    файлов;
                                                                </li>
                                                                <li>слабые конфигурации .htaccess, которые можно
                                                                    обойти;
                                                                </li>
                                                                <li>наличие файлов резервных копий, раскрывающих
                                                                    конфиденциальную информацию (раскрытие исходного
                                                                    кода);
                                                                </li>
                                                                <li>Shellshock;</li>
                                                                <li>открытые перенаправления;</li>
                                                                <li>нестандартные методы HTTP, которые могут быть
                                                                    разрешены (PUT).
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=\"form-container audit_steps\">
                                        <h2 class=\"container h1 wow fadeIn full-opacity pr-0 pl-0\"
                                            data-wow-duration=\".5s\"
                                            style=\"visibility: hidden; animation-duration: 0.5s; animation-name: none;\">
                                            <span>Этапы работы по аудиту безопасности сайта</span>
                                        </h2>

                                        <div class=\"u-promo-work-list__container flex --align-stretch\" style=\"margin-left: 8em; width: 90%\">
                                            <div class=\"row\">
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num1.svg"), "html", null, true);
        echo "\" alt=\"1\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Анализ степени защиты административных
                                                                данных</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num2.svg"), "html", null, true);
        echo "\" alt=\"2\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Проверка на устойчивость к PHP, SQL инъекциям
                                                                (внедрениям кода) и XSS/CSRF атакам</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num3.svg"), "html", null, true);
        echo "\" alt=\"3\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Анализ на безопасность всех форм (авторизации,
                                                                регистрации, поиска и др.)</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num4.svg"), "html", null, true);
        echo "\" alt=\"4\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Проверка на редиректы</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num5.svg"), "html", null, true);
        echo "\" alt=\"5\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Стойкость к подбору логинов и паролей</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num6.svg"), "html", null, true);
        echo "\" alt=\"6\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Поиск расширений с известными уязвимостями</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num7.svg"), "html", null, true);
        echo "\" alt=\"7\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Аудит возможности легкого получения секретной
                                                                информации</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=\"col-md-6 mb-32 --align-start\">
                                                    <div class=\"u-promo-work-list__inner --bg-gray\">
                                                        <div class=\"--num\">
                                                            <img src=\"";
        // line 286
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/numbers/num8.svg"), "html", null, true);
        echo "\" alt=\"8\">
                                                        </div>
                                                        <div class=\"--text\">
                                                            <strong>Поиск незащищенных мест на сервере и его
                                                                веб-окружении</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "landing/pages/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 286,  347 => 275,  334 => 265,  321 => 255,  308 => 245,  294 => 234,  280 => 223,  266 => 212,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/pages/about.html.twig", "/symfony/templates/landing/pages/about.html.twig");
    }
}
