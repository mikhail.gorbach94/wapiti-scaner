<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/blocks/contacts.html.twig */
class __TwigTemplate_ee85b500219e62119bfeebb75c9ddc1bdcbb2c1c519215e2e8cbcdc54bf93b51 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"rec262927501\" class=\"r t-rec t-rec_pt_0\" style=\"padding-top:0px; \" data-animationappear=\"off\">
    <div class=\"t-cover\" id=\"recorddiv262927501\" bgimgfield=\"img\"
         style=\"background-image:url('https://static.tildacdn.com/tild3630-3138-4162-b338-373233376633/-/resize/20x/Network-Zone.jpg');\">
        <div class=\"t-cover__carrier loaded\" id=\"coverCarry262927501\"
             data-content-cover-bg=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/Network-Zone.jpg"), "html", null, true);
        echo "\"
             data-content-cover-parallax=\"\"
             style=\"background-attachment: scroll; background-image: url(";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/Network-Zone.jpg"), "html", null, true);
        echo ");\"></div>
        <div class=\"t-cover__filter\"
             style=\"background-image: -moz-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.50));background-image: -webkit-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.50));background-image: -o-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.50));background-image: -ms-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.50));background-image: linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.50));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#190f0f0f', endColorstr='#7f0f0f0f');\"></div>
        <div class=\"t698\">
            <div class=\"t-container \">
                <div class=\"t-width t-width_8 t698__mainblock\">
                    <div class=\"t-cover__wrapper t-valign_middle\">
                        <div class=\"t698__mainwrapper\" data-hook-content=\"covercontent\">
                            <h2 class=\"t698__title t-title t-title_xs\"
                                style=\"color:#f3f3f9;font-size:40px;font-weight:700;\" field=\"title\">
                                <div style=\"font-size:40px;\" data-customstyle=\"yes\">Есть вопросы?</div>
                            </h2>
                            <div class=\"t698__descr t-descr t-descr_md\"
                                 style=\"color:#f3f3f9;font-size:20px;font-weight:500;\" field=\"descr\">Закажите звонок
                                специалиста
                            </div>
                            <div>
                                <div id=\"form262927501\" role=\"form\"
                                     data-formactiontype=\"2\" data-inputbox=\".t-input-group\"
                                     class=\"t-form t-form_inputs-total_2\">
                                    <div class=\"js-successbox t-form__successbox t-text t-text_md\"
                                         style=\"display:none;\">Спасибо! Данные успешно отправлены.
                                    </div>
                                    <div class=\"js-errorbox t-form__errorbox-wrapper t-text t-text_md\"
                                         style=\"display:none;\">Не удалось отправить данные, повторите позднее.
                                    </div>
                                    <div class=\"t-form__inputsbox\">
                                        <div class=\"t-input-group t-input-group_nm\">
                                            <div class=\"t-input-block\" id=\"name\"><input type=\"text\" autocomplete=\"name\"
                                                                                        name=\"Name\" class=\"t-input\"
                                                                                        value=\"\" placeholder=\"Имя\"
                                                                                        style=\"color:#484848; border:0px solid #ffffff; background-color:#f3f3f9; border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;\">
                                                <div class=\"t-input-error\"></div>
                                            </div>
                                        </div>
                                        <div class=\"t-input-group t-input-group_ph\">
                                            <div class=\"t-input-block\" id=\"tel\"><input type=\"tel\" autocomplete=\"tel\"
                                                                                       name=\"Phone\"
                                                                                       class=\"t-input\"
                                                                                       value=\"\"
                                                                                       placeholder=\"Номер телефона\"
                                                                                       data-inputmask=\"'mask': '+7 999 999-99-99'\"
                                                                                       style=\"color:#484848; border:0px solid #ffffff; background-color:#f3f3f9; border-radius: 0px; -moz-border-radius: 0px; -webkit-border-radius: 0px;\">
                                                <div class=\"t-input-error\"></div>
                                            </div>
                                        </div>

                                        <div class=\"t-form__submit\">
                                            <button type=\"submit\" class=\"t-submit\" id=\"form262927501_submit\"
                                                    style=\"color:#0f0f0f;background-color:#ffd200;border-radius:0px; -moz-border-radius:0px; -webkit-border-radius:0px;\">
                                                Заказать
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=\"t698__form-bottom-text t-text t-text_xs\" field=\"text\">*нажимая на кнопку, Вы
                                даете согласие на <a href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("privacy-policy");
        echo "\"
                                                     style=\"color:#ffffff !important;text-decoration: none;border-bottom: 1px solid #ffffff;box-shadow: inset 0px -0px 0px 0px #ffffff;-webkit-box-shadow: inset 0px -0px 0px 0px #ffffff;-moz-box-shadow: inset 0px -0px 0px 0px #ffffff;\"
                                                     target=\"_blank\" rel=\"nofollow noopener\">обработку персональных
                                    данных</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "landing/blocks/contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 64,  48 => 7,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/blocks/contacts.html.twig", "/symfony/templates/landing/blocks/contacts.html.twig");
    }
}
