<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/blocks/header.html.twig */
class __TwigTemplate_b2a01b6e0ad182a6e39908641cd8159784ed6e259fb82473ba67c550bb082e58 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header class=\"b-stripe b-stripe_type_overhang b-stripe_content_header\">
    <div class=\"b-stripe__in\">
        <!-- Mobile menu -->
        <div class=\"b-stripe b-stripe_content_mheader\">
            <div class=\"b-stripe__in\">
                <div class=\"b-content-columns b-content-columns_content_mheader\">
                    <div class=\"b-content-column b-content-column_layout_a\">
                        <div class=\"b-content-column__in\">
                            <a href=\"/\" class=\"b-logo__image\"></a>
                        </div>
                    </div>

                    <div class=\"b-content-column b-content-column_layout_c\">
                        <div class=\"b-content-column__in\">
                            <div class=\"b-callback b-callback_content_phones b-i b-i_content_callback-phones b-callback_viewtype_not-callback-phones\">

                                <div class=\"b-callback__number b-callback__number_type_main\">
                                    <div class=\"b-callback__number-text\">
                                        <a href=\"tel:+74952300303\" id=\"\" class=\"b-menu__item-text phone-header\">+7&nbsp;495&nbsp;230&nbsp;03&nbsp;03</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"b-content-column b-content-column_layout_d\">
                        <div class=\"b-content-column__in\">
                            <div class=\"b-content-columns b-content-columns_content_fast-access\">
                                <div class=\"b-content-column b-content-column_layout_b\">
                                    <div class=\"b-content-column__in\">
                                        <div class=\"b-wrapper-hamburger\">
                                            <div class=\"b-nav-icon b-nav-icon_type_hamburger\">
                                                <div class=\"b-nav-icon__item\"></div>
                                                <div class=\"b-nav-icon__item\"></div>
                                                <div class=\"b-nav-icon__item\"></div>
                                                <div class=\"b-nav-icon__item\"></div>
                                                <div class=\"b-nav-icon__item\"></div>
                                                <div class=\"b-nav-icon__item\"></div>
                                                <div class=\"b-external-link\"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"b-modal-windows\">
                    <div class=\"b-stripe b-stripe_content_main-menu-320 b-popup b-popup_content_main-menu-320 resise-a\"
                         style=\"height: 363px;\">
                        <div class=\"b-stripe__in\">
                            <div class=\"b-section b-section_content_main-menu-320\">
                                <div class=\"b-section__header\">
                                    <div class=\"b-content-columns b-content-columns_content_main-menu-320\">
                                        <div class=\"b-content-column b-content-column_layout_a\">
                                            <div class=\"b-content-column__in\">
                                                <a href=\"tel:+74952300303\" id=\"\"
                                                   class=\"b-phone b-phone_content_main-menu-320 b-link\">+7&nbsp;495&nbsp;230&nbsp;03&nbsp;03</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"b-section__body\">
                                    <nav class=\"b-menu b-menu_content_mobile\">
                                        <ul class=\"b-menu__list\">

                                            <li class=\"b-menu__item \">
                                                <a class=\"b-menu__item-text\" href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("about");
        echo "\"
                                                   title=\"О сервисе\">
                                                    О сервисе
                                                </a>
                                            </li>

                                            <li class=\"b-menu__item \">
                                                <a class=\"b-menu__item-text\" href=\"";
        // line 76
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contacts");
        echo "\"
                                                   title=\"Контакты\">
                                                    Контакты
                                                </a>
                                            </li>

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Mobile menu -->
        <!-- Desktop menu -->
        <div class=\"b-stripe b-stripe_content_bheader\">
            <div class=\"b-stripe__in\">
                <div class=\"b-content-columns b-content-columns_content_section-menu\">
                    <div class=\"b-content-column b-content-column_layout_a\">
                        <div class=\"b-content-column__in\">
                    <span class=\"b-logo b-logo_content_softline-header-desktop\">
                        <a href=\"/\" class=\"b-logo__image\"></a>
                    </span>
                        </div>
                    </div>

                    <div class=\"b-content-column b-content-column_layout_b\">
                        <div class=\"b-content-column__in\">
                            <nav class=\"b-menu b-menu_content_section\">
                                <ul class=\"b-menu__list\">

                                    <li class=\"b-menu__item\">
                                        <a class=\"b-menu__item-text\" href=\"";
        // line 110
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("about");
        echo "\" title=\"О сервисе\">
                                            О сервисе
                                        </a>
                                    </li>


                                    <li class=\"b-menu__item \">
                                        <a class=\"b-menu__item-text\" href=\"";
        // line 117
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contacts");
        echo "\"
                                           title=\"Контакты\">
                                            Контакты
                                        </a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>

                    <div class=\"b-content-column b-content-column_layout_c\">
                        <div class=\"b-content-column__in\">
                            <a href=\"tel:+74952300303\" id=\"\" class=\"b-menu__item-text phone-header\">+7&nbsp;495&nbsp;230&nbsp;03&nbsp;03</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Desktop menu -->
    </div>
</header>
";
    }

    public function getTemplateName()
    {
        return "landing/blocks/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 117,  154 => 110,  117 => 76,  107 => 69,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/blocks/header.html.twig", "/symfony/templates/landing/blocks/header.html.twig");
    }
}
