<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mail/call.html.twig */
class __TwigTemplate_4acafaedfde0895851844bd79cec5d04007b5b732b0cd98e49a66f5b90ec43c9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <title>Заказ звонка специалиста</title>
</head>
<body>
<div>
    <p>Заявка оставлена на сайте: ";
        // line 10
        echo twig_escape_filter($this->env, ($context["host"] ?? null), "html", null, true);
        echo "</p>
    <p>Адрес страницы, с которой оставлена заявка: ";
        // line 11
        echo twig_escape_filter($this->env, ($context["host"] ?? null), "html", null, true);
        echo "</p>
    <p>Отдел, в который распределяется заявка=d5</p>
</div>
<div>
    <h4>Данные о клиенте:</h4>
    <p>ФИО: ";
        // line 16
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "</p>
    <p>Телефон: ";
        // line 17
        echo twig_escape_filter($this->env, ($context["phone"] ?? null), "html", null, true);
        echo "</p>
</div>
<div>
    <h4>Данные о клиенте, определенные по ip-адресу:</h4>
    <p>Страна: ";
        // line 21
        echo twig_escape_filter($this->env, ($context["country"] ?? null), "html", null, true);
        echo "</p>
    <p>Регион: ";
        // line 22
        echo twig_escape_filter($this->env, ($context["region"] ?? null), "html", null, true);
        echo "</p>
    <p>Город: ";
        // line 23
        echo twig_escape_filter($this->env, ($context["city"] ?? null), "html", null, true);
        echo "</p>
</div>
</body>";
    }

    public function getTemplateName()
    {
        return "mail/call.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 23,  75 => 22,  71 => 21,  64 => 17,  60 => 16,  52 => 11,  48 => 10,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "mail/call.html.twig", "/symfony/templates/mail/call.html.twig");
    }
}
