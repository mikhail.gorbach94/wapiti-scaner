<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/blocks/possibilities.html.twig */
class __TwigTemplate_48a92d440586874721b130bd095d4c78f9866486ae9e5cac9104d7b5cf136c29 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"r t-rec t-rec_pt_75 t-rec_pb_0\"
     style=\"padding-top:75px;padding-bottom:0px;background-color:#ffffff; \" data-record-type=\"60\"
     data-bg-color=\"#ffffff\" data-animationappear=\"off\">
    <div class=\"t050\">
        <div class=\"t-container t-align_center\">
            <div class=\"t-col t-col_12 \"><h2 class=\"t050__title t-title t-title_xxl\" field=\"title\"
                                             style=\"color:#0f0f0f;font-size:40px;font-weight:700;font-family:'Montserrat';\">
                    Возможности сканера</h2>
            </div>
        </div>
    </div>
</div>
<div class=\"r t-rec t-rec_pt_45 t-rec_pb_75\"
     style=\"padding-top:45px;padding-bottom:75px; \" data-record-type=\"503\">
    <div class=\"t503\">
        <div class=\"t-container\">
            <div class=\"t503__col t-col t-col_4 t-item t-animate t-animate__chain_first-in-row t-animate_started t-animate__chain_showed\"
                 data-animate-style=\"fadein\" data-animate-chain=\"yes\" data-animate-start-time=\"1633458695773\"
                 style=\"transition-delay: 0s;\">
                <div class=\"t503__content t-align_left\">
                    <div class=\"t503__wrapper\"><img
                                src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/icons/check.svg"), "html", null, true);
        echo "\"
                                class=\"t503__img t-img loading\" imgfield=\"li_img__1476978617855\" style=\"width:50px;\">
                        <div class=\"t503__title t-name t-name_md\" style=\"color:#0f0f0f;font-size:20px;font-weight:700;\"
                             field=\"li_title__1476978617855\">Проверка открытых портов
                        </div>
                    </div>
                    <div class=\"t503__descr t-descr t-descr_xs\"
                         style=\"color:#0f0f0f;font-size:14px;font-weight:400;font-family:'Montserrat';\"
                         field=\"li_descr__1476978617855\">Проверяем 0-65535 сетевых портов и находим все доступные приложения
                    </div>
                </div>
            </div>
            <div class=\"t503__col t-col t-col_4 t-item t-animate t-animate_started\" data-animate-style=\"fadein\"
                 data-animate-chain=\"yes\" data-animate-start-time=\"1633458695933\" style=\"transition-delay: 0.16s;\">
                <div class=\"t503__content t-align_left\">
                    <div class=\"t503__wrapper\"><img
                                src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/icons/wrench.svg"), "html", null, true);
        echo "\"
                                class=\"t503__img t-img loading\" imgfield=\"li_img__1476978621977\" style=\"width:50px;\">
                        <div class=\"t503__title t-name t-name_md\" style=\"color:#0f0f0f;font-size:20px;font-weight:700;\"
                             field=\"li_title__1476978621977\">Отображение уязвимостей
                        </div>
                    </div>
                    <div class=\"t503__descr t-descr t-descr_xs\"
                         style=\"color:#0f0f0f;font-size:14px;font-weight:400;font-family:'Montserrat';\"
                         field=\"li_descr__1476978621977\">Для всех сервисов проверяем наличие уязвимостей и эксплойтов по базе CVEdetails
                    </div>
                </div>
            </div>
            <div class=\"t503__col t-col t-col_4 t-item t-animate t-animate_started\" data-animate-style=\"fadein\"
                 data-animate-chain=\"yes\" data-animate-start-time=\"1633458696094\" style=\"transition-delay: 0.32s;\">
                <div class=\"t503__content t-align_left\">
                    <div class=\"t503__wrapper\"><img
                                src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/icons/magic-wand.svg"), "html", null, true);
        echo "\"
                                class=\"t503__img t-img loading\" imgfield=\"li_img__1476978655843\" style=\"width:50px;\">
                        <div class=\"t503__title t-name t-name_md\" style=\"color:#0f0f0f;font-size:20px;font-weight:700;\"
                             field=\"li_title__1476978655843\">Проверка кода на XSS и SQLI
                        </div>
                    </div>
                    <div class=\"t503__descr t-descr t-descr_xs\"
                         style=\"color:#0f0f0f;font-size:14px;font-weight:400;font-family:'Montserrat';\"
                         field=\"li_descr__1476978655843\">Сканер найдет все формы на сайте и проверит их на SQLi, XSS, и другие атаки OWASP TOP10
                    </div>
                </div>
            </div>
            <div class=\"t-clear t503__separator\" style=\"margin-bottom:30px;\"></div>
            <div class=\"t503__col t-col t-col_4 t-item t-animate t-animate__chain_first-in-row t-animate_started t-animate__chain_showed\"
                 data-animate-style=\"fadein\" data-animate-chain=\"yes\" data-animate-start-time=\"1633459434295\"
                 style=\"transition-delay: 0s;\">
                <div class=\"t503__content t-align_left\">
                    <div class=\"t503__wrapper\"><img
                                src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/icons/key.svg"), "html", null, true);
        echo "\"
                                class=\"t503__img t-img loading\" imgfield=\"li_img__1476978737724\" style=\"width:50px;\">
                        <div class=\"t503__title t-name t-name_md\" style=\"color:#0f0f0f;font-size:20px;font-weight:700;\"
                             field=\"li_title__1476978737724\">Подбор паролей к учетным записям
                        </div>
                    </div>
                    <div class=\"t503__descr t-descr t-descr_xs\"
                         style=\"color:#0f0f0f;font-size:14px;font-weight:400;font-family:'Montserrat';\"
                         field=\"li_descr__1476978737724\">Брутфорсим по 40 протоколам, используя словари ботнетов и утечки из реальных парольных баз
                    </div>
                </div>
            </div>
            <div class=\"t503__col t-col t-col_4 t-item t-animate t-animate_started\" data-animate-style=\"fadein\"
                 data-animate-chain=\"yes\" data-animate-start-time=\"1633459434458\" style=\"transition-delay: 0.16s;\">
                <div class=\"t503__content t-align_left\">
                    <div class=\"t503__wrapper\"><img
                                src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/icons/shield.svg"), "html", null, true);
        echo "\"
                                class=\"t503__img t-img loading\" imgfield=\"li_img__1476978755814\" style=\"width:50px;\">
                        <div class=\"t503__title t-name t-name_md\" style=\"color:#0f0f0f;font-size:20px;font-weight:700;\"
                             field=\"li_title__1476978755814\">Отображение уязвимостей в популярных CMS<br></div>
                    </div>
                    <div class=\"t503__descr t-descr t-descr_xs\"
                         style=\"color:#0f0f0f;font-size:14px;font-weight:400;font-family:'Montserrat';\"
                         field=\"li_descr__1476978755814\">Найдем уязвимости в Wordpress, Drupal и установленных плагинах
                    </div>
                </div>
            </div>
            <div class=\"t503__col t-col t-col_4 t-item t-animate t-animate_started\" data-animate-style=\"fadein\"
                 data-animate-chain=\"yes\" data-animate-start-time=\"1633459434619\" style=\"transition-delay: 0.32s;\">
                <div class=\"t503__content t-align_left\">
                    <div class=\"t503__wrapper\"><img
                                src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/icons/eye.svg"), "html", null, true);
        echo "\"
                                class=\"t503__img t-img loading\" imgfield=\"li_img__1476978778074\" style=\"width:50px;\">
                        <div class=\"t503__title t-name t-name_md\" style=\"color:#0f0f0f;font-size:20px;font-weight:700;\"
                             field=\"li_title__1476978778074\">Поиск забытых поддоменов <br></div>
                    </div>
                    <div class=\"t503__descr t-descr t-descr_xs\"
                         style=\"color:#0f0f0f;font-size:14px;font-weight:400;font-family:'Montserrat';\"
                         field=\"li_descr__1476978778074\">Найдем поддомены и ip компании, которые злоумышленники могут атаковать
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "landing/blocks/possibilities.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 103,  138 => 88,  119 => 72,  98 => 54,  79 => 38,  60 => 22,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/blocks/possibilities.html.twig", "/symfony/templates/landing/blocks/possibilities.html.twig");
    }
}
