<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/blocks/footer.html.twig */
class __TwigTemplate_913f7d38c9f451713b19e23c3a137cf34e26c17bf8c5df58081b68628efe2368 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer class=\"b-stripe b-stripe_content_footer b-stripe_type_overhang\">
    <div class=\"b-stripe__in\">
        <div class=\"b-stripe b-stripe_content_tfooter\">
            <div class=\"b-stripe__in\">
                <div class=\"b-content-columns b-content-columns_content_tfooter\">
                    <div class=\"b-content-column b-content-column_layout\">
                        <div class=\"b-content-column__in\">
                            <span class=\"b-logo__image__footer\"></span>
                        </div>
                    </div>

                    <div class=\"b-content-column b-content-column_layout_b\">
                        <nav class=\"row b-menu b-menu_content_footer mb-48\">

                            <div class=\"col-lg-3 col-md-6 b-menu__section\">
                                <div class=\"title__footer color-yellow\">Сервис</div>
                                <ul class=\"b-menu__list\">
                                    <li class=\"b-menu__item \">
                                        <a class=\"b-menu__item-text b-link\" href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("about");
        echo "\">О сервисе</a>
                                    </li>
                                </ul>
                            </div>

                            <div class=\"col-lg-3 col-md-6  b-menu__section\">
                                <div class=\"title__footer color-yellow\">Контакты</div>
                                <ul class=\"b-menu__list\">
                                    <li class=\"b-menu__item \">
                                        <a href=\"tel:+74952300303\" class=\"b-menu__item-text phone-header\"
                                           style=\"font-weight: 600;\">+7&nbsp;495&nbsp;230&nbsp;03&nbsp;03</a>
                                    </li>
                                    <li class=\"b-menu__item \">
                                        <a href=\"mailto:efsol@efsol.ru\" class=\"b-menu__item-text b-link mail__footer\"
                                           onmouseover=\"\$(this).attr('href', 'mailto:efsol@efsol.ru')\">efsol@efsol.ru</a>
                                    </li>
                                </ul>
                                <div class=\"social-links flex-block\">
                                    <a href=\"//www.linkedin.com/company/efsol-ru/\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--in.svg"), "html", null, true);
        echo "\" alt=\"\"
                                                class=\"social-links__logo\"></a>
                                    <a href=\"//www.instagram.com/efsol.ru/\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--insta.svg"), "html", null, true);
        echo "\"
                                                alt=\"\" class=\"social-links__logo\"></a>
                                    <a href=\"//www.facebook.com/efsol.ru\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--fb.svg"), "html", null, true);
        echo "\" alt=\"\"
                                                class=\"social-links__logo\"></a>
                                    <a href=\"//www.youtube.com/channel/UCB0npNV2HyZgb3lOLh11D0w\" rel=\"nofollow\"
                                       target=\"_blank\" class=\"social-links__link\"><img
                                                src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--youtube.svg"), "html", null, true);
        echo "\"
                                                alt=\"\" class=\"social-links__logo\"></a>
                                    <a href=\"//vk.com/efsolofficial\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--vk.svg"), "html", null, true);
        echo "\" alt=\"\"
                                                class=\"social-links__logo\"></a>
                                    <a href=\"//t.me/efsolru\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--tg.svg"), "html", null, true);
        echo "\" alt=\"\"
                                                class=\"social-links__logo\"></a>
                                    <a href=\"//zen.yandex.ru/id/5f73541cb7327a61f466fab8\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--zen.svg"), "html", null, true);
        echo "\"
                                                alt=\"\" class=\"social-links__logo\"></a>
                                    <a href=\"//ok.ru/group/58206054580239\" rel=\"nofollow\" target=\"_blank\"
                                       class=\"social-links__link\"><img
                                                src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/social/social-icon--ok.svg"), "html", null, true);
        echo "\" alt=\"\"
                                                class=\"social-links__logo\"></a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>

                <div class=\"b-content-columns b-content-columns_content_bfooter\">
                    <div class=\"b-content-column b-content-column_layout_b\">
                        <div class=\"b-content-column__in\">
                            <ul class=\"b-list b-list_content_3-w160\">
                                <li class=\"b-list__item\">
                                    <div class=\"b-copyright-special-place\">
                                        <a class=\"b-copyright-special-place__text b-link\"
                                           href=\"";
        // line 82
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("privacy-policy");
        echo "\"
                                           title=\"Политика конфиденциальности\">Политика конфиденциальности</a>
                                    </div>
                                </li>

                                <li class=\"b-list__item\">
                                    <span class=\"b-copyrigth\">©&nbsp;EFSOL&nbsp;";
        // line 88
        echo twig_escape_filter($this->env, ($context["year"] ?? null), "html", null, true);
        echo ".&nbsp;Все права защищены</span>
                                </li>

                                <li class=\"b-list__item\">
                                    <div class=\"b-copyright-special-place\">
                                        <a class=\"b-copyright-special-place__text b-link\" href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cookie");
        echo "\"
                                           title=\"Временные файлы cookie\">Временные файлы cookie</a>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class=\"b-content-column b-content-column_layout_c\">
                        <div class=\"b-content-column__in\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "landing/blocks/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 93,  156 => 88,  147 => 82,  129 => 67,  122 => 63,  115 => 59,  108 => 55,  101 => 51,  94 => 47,  87 => 43,  80 => 39,  57 => 19,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/blocks/footer.html.twig", "/symfony/templates/landing/blocks/footer.html.twig");
    }
}
